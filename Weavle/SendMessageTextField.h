//
//  SendMessageTextField.h
//  Weavle
//
//  Created by Thomas Laskowski on 2015-10-15.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SendMessageTextField : UITextField
@property (nonatomic,assign) BOOL canResign;
@end
