//
//  HomepageViewController.h
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-14.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomepageViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *labelNeighbourhood;

@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *province;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLocationResolve;

@end
