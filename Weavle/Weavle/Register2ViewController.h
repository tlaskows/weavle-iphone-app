//
//  Register2ViewController.h
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-02.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterViewController.h"

@interface Register2ViewController : UIViewController

@property (strong, nonatomic) RegisterViewController *registerViewController;
@property (weak, nonatomic) IBOutlet UIButton *buttonRegister;
@property (weak, nonatomic) IBOutlet UILabel *labelNeighbourhood;

@end
