//
//  WeavleThreadTableCell.h
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-21.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeavleThreadTableCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *labelText;
@property (strong, nonatomic) IBOutlet UIImageView *picture;

@end
