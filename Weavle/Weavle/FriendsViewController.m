//
//  FriendsViewController.m
//  Weavle
//
//  Created by Thomas Laskowski on 2015-10-22.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import "FriendsViewController.h"
#import "FriendCollectionViewCell.h"

@interface FriendsViewController ()
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@end

@implementation FriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self.collectionView registerClass:[FriendCollectionViewCell class] forCellWithReuseIdentifier:@"FriendCollectionViewCell"];
    
    self.dataArray = [[NSMutableArray alloc] init];
    
    // Do any additional setup after loading the view.
    [self.dataArray addObject:@"Tom"];
    [self.dataArray addObject:@"Peter"];
    [self.dataArray addObject:@"Elvin"];
    [self.dataArray addObject:@"Dannie"];
    [self.dataArray addObject:@"Shy Ronnie"];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *data = [self.dataArray objectAtIndex:indexPath.row+indexPath.section/3];
    
//    NSString *cellData = [data objectAtIndex:indexPath.row];
    
    static NSString *cellIdentifier = @"FriendCollectionViewCell";
    
    FriendCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    //UILabel *titleLabel = [cell];
    
    //[titleLabel setText:cellData];
    
    cell.labelName.text = data;
    
    return cell;
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.dataArray count];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
