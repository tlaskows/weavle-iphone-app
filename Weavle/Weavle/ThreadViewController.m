//
//  ThreadViewController.m
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-21.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import "ThreadViewController.h"
#import "WeavleThreadTableCell.h"
#import "AppDelegate.h"

@interface ThreadViewController ()

@property (strong, nonatomic) IBOutlet UITableView *threadTableView;

@property (strong, nonatomic) IBOutlet UITextField *threadTextField;
@property (readwrite, assign) int serverResponseTimeoutCount;
@property (strong, nonatomic) AppDelegate *app;
@end

@implementation ThreadViewController

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.threadArray = [[NSMutableArray alloc] init];

    // TODO test code to see how threads are formatted... remove soon
    
    NSString * threadsString=[NSString stringWithFormat:@"http://www.thomaslaskowski.com:6969/weavle/v1/users/%d/message_threads/inbox?pageNumber=0&pageSize=10", self.app.userId];
    
    [self.app.postHelper sendPostRequest: threadsString withJSONdata: nil withType:@"GET"];
    
    NSString * testString;
    
    testString = @"test";
    
    testString = @"test2";
    
    self.serverResponseTimeoutCount = 0;
    
    NSTimer *serverTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval: 0.2
                                                                   target: self
                                                                 selector:@selector(onServerResponseCheck:)
                                                                 userInfo: nil repeats:YES];
}

-(void)onServerResponseCheck:(NSTimer *)timer
{
    //  check to see if we are done waiting for the location
    if ([self.app.postHelper isDoneReceiving] == YES)
    {
        NSLog(@"GOT A RESPONSE FROM THE SERVER!!!");
        
        [timer invalidate];
        
        // look at the response
        NSMutableData *serverResponseData = [self.app.postHelper getResponseData];
        
        NSError* error;
        NSDictionary* serverResponseJSON = [NSJSONSerialization JSONObjectWithData:serverResponseData
                                                                           options:kNilOptions
                                                                             error:&error];
        
        // TODO: if parsing the server reponse doesn't work, do something here...

        NSArray *threads = [serverResponseJSON objectForKey:@"conversations"];
        
        for (int i=0; i<[threads count]; i++)
        {
            NSDictionary *thread = threads[i];
            
            [self.threadArray addObject:thread];
        }
        [self.threadTableView reloadData];
    }
    else
    {
        self.serverResponseTimeoutCount++;
    }
    
    // wait up to 2 seconds for the server to return response data
    if (self.serverResponseTimeoutCount >= 10)
    {
        NSLog(@"The server timed out...");
        
        [timer invalidate];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"The Weavle server has timed out."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
}

- (IBAction)buttonStartThreadClicked:(id)sender
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Create thread" message:@"Please enter thread name:" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Create", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    self.threadTextField = [alert textFieldAtIndex:0];
    self.threadTextField.keyboardType = UIKeyboardTypeDefault;
    self.threadTextField.placeholder = @"Enter thread name";
    [alert show];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        // Custom initialization
        
        if (!self.app)
        {
            self.app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        }
    }
    
    return self;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString* detailString = self.threadTextField.text;
    NSLog(@"String is: %@", detailString); //Put it on the debugger
    if ([self.threadTextField.text length] <= 0 || buttonIndex == 0){
        return; //If cancel or 0 length string the string doesn't matter
    }
    if (buttonIndex == 1)
    {
        NSLog(@"Button create clicked!\n");
        
        self.app.selectParticipantsViewController.threadName = detailString;
        
        [self.app changeRootViewController:self.app.selectParticipantsViewController];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.threadArray = [[NSMutableArray alloc] init];
    
   // NSArray* arrayItem = [NSArray arrayWithObjects:<#(id), ...#>, nil]
    
/*    [self.threadArray addObject:@"Soccer game"];
    [self.threadArray addObject:@"Tennis game"];
    [self.threadArray addObject:@"Baseball game"];
    [self.threadArray addObject:@"Football game"];
    [self.threadArray addObject:@"Squash game"];
    [self.threadArray addObject:@"Basketball game"];*/
    
//    self.threadArray = @[@{@"description":@"Soccer game", @"isChecked":@"false"}];
/*    NSMutableDictionary *tableRow = [[NSMutableDictionary alloc] init];
    
    [tableRow setObject:@"Soccer game" forKey:@"description"];
    [tableRow setObject:@"false" forKey:@"isChecked"];
    [self.threadArray addObject:tableRow];

    //[tableRow removeAllObjects];

    tableRow = [[NSMutableDictionary alloc] init];
    [tableRow setObject:@"Tennis game" forKey:@"description"];
    [tableRow setObject:@"false" forKey:@"isChecked"];
    [self.threadArray addObject:tableRow];

    tableRow = [[NSMutableDictionary alloc] init];
    [tableRow setObject:@"Baseball game" forKey:@"description"];
    [tableRow setObject:@"false" forKey:@"isChecked"];
    [self.threadArray addObject:tableRow];*/
    
    //[self.threadTableView reloadData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.threadArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WeavleThreadTableCell *messageCell = [tableView dequeueReusableCellWithIdentifier:@"ThreadViewCell" forIndexPath:indexPath];
    [self configureCell:messageCell forIndexPath:indexPath];
    
    return messageCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //return self.cellHeight+20;
    return 42;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)path
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:path];
    
    /*    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
     cell.accessoryType = UITableViewCellAccessoryNone;
     } else {
     cell.accessoryType = UITableViewCellAccessoryCheckmark;
     }*/
    
    NSMutableDictionary *cellItem;
    
    cellItem = [self.threadArray objectAtIndex:path.row];
    
    self.app.chatViewController.threadId = [[cellItem objectForKey:@"id"] integerValue];
    
    [self.app changeRootViewController:self.app.chatViewController];

}


#pragma mark Method to configure the appearance of a message list prototype cell

- (void)configureCell:(WeavleThreadTableCell *)messageCell forIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *chatMessage = self.threadArray[indexPath.row];
    
    messageCell.labelText.text = [chatMessage objectForKey:@"title"];
    
    //messageCell.textLabel.frame = CGRectMake(51, 10, 261, 24);
    
    
    /*  if ([[chatMessage senderId] isEqualToString:self.myUserId]) {
     // If the message was sent by myself
     messageCell.chatMateMessageLabel.text = @"";
     messageCell.myMessageLabel.text = chatMessage.text;
     } else {
     // If the message was sent by the chat mate
     messageCell.myMessageLabel.text = @"";
     messageCell.chatMateMessageLabel.text = chatMessage.text;
     }*/

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
