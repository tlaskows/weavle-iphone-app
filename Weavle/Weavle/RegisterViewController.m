//
//  ViewControllerRegisterViewController.m
//  Weavle
//
//  Created by Thomas Laskowski on 2015-08-27.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import "RegisterViewController.h"
#import "WeavleLocationManager.h"
#import "LoginViewController.h"
#import "Register2ViewController.h"

#import "AppDelegate.h"

@interface RegisterViewController ()

@property (strong, nonatomic) WeavleLocationManager *weavleLocationManager;
@property (strong, nonatomic) AppDelegate *app;

@end


@implementation RegisterViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        // Custom initialization
        if (!self.app)
        {
            self.app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        }
    }
    
    return self;
}

- (NSString *) getBirthdate
{
    return self.textFieldBirthdate.text;
}

- (NSString *) getGender
{
    if (self.selectorGender.selectedSegmentIndex == 0)
    {
        return @"M";
    }
    else
    {
        return @"F";
    }
}

- (IBAction)buttonRegisterNext:(id)sender
{
    // check for blank fields
    if ([self.textFieldUsername.text  isEqual: @""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"The username field cannot be blank"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    else if ([self.textFieldPassword.text isEqual: @""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"The password field cannot be blank"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    else if ([self.textFieldEmail.text isEqual: @""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"The email field cannot be blank"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    else if ([self.textFieldBirthdate.text isEqual: @""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"The birthdate field cannot be blank"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }


    
    self.activityLocationResolve.hidden = NO;
    [self.activityLocationResolve startAnimating];
    
    // TODO: override init and put this in there
    self.weavleLocationManager = [[WeavleLocationManager alloc] init];
    
    [self.weavleLocationManager startToGetCurrentLocation:sender];
    
    NSLog(@"isDoneResolving: %d", [self.weavleLocationManager isDoneResolving]);
    
    NSTimer *locationResolveTimer = [NSTimer scheduledTimerWithTimeInterval: 0.2
                                                  target: self
                                                selector:@selector(onLocationResolveCheck:)
                                                userInfo: nil repeats:YES];
}

-(void)onLocationResolveCheck:(NSTimer *)timer
{
    //  check to see if we are done waiting for the location
    if ([self.weavleLocationManager isDoneResolving] == YES)
    {
        NSLog(@"DONE RESOLVING LOCATION!!!");
        
        NSLog(@"Longitute is: %@", [self.weavleLocationManager getLongitude]);
        self.longitude = [self.weavleLocationManager getLongitude];
        
        NSLog(@"Latitude is: %@", [self.weavleLocationManager getLatitude]);
        self.latitude = [self.weavleLocationManager getLatitude];
        
        NSLog(@"City is: %@", [self.weavleLocationManager getCity]);
        self.city = [self.weavleLocationManager getCity];
        
        NSLog(@"Province/State is: %@", [self.weavleLocationManager getProvinceOrState]);
        self.province = [self.weavleLocationManager getProvinceOrState];
        
        NSLog(@"Country is: %@", [self.weavleLocationManager getCountry]);
        self.country = [self.weavleLocationManager getCountry];
        
        [timer invalidate];
        
        // TODO: This is just some test code, remove and put it in the right place
        NSDictionary *locationData =
        @{@"city" : @"Toronto",
          @"province" : @"ON",
          @"country" : @"Canada",
        };
        
        NSDictionary *registrationData =
        @{@"username": self.textFieldUsername.text,
          @"neighbourhood" : locationData,
        };
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:registrationData options:NSJSONWritingPrettyPrinted error:&error];
        
        if (!jsonData)
        {
            NSLog(@"Error creating JSON data %@", error);
        }
        else
        {
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            NSLog(@"JSON data is %@: ", jsonString);
            
            NSString *neighbourhood = [NSString stringWithFormat:@"%@\n%@\n%@", self.city, self.province, self.country];
            
            [self.activityLocationResolve stopAnimating];
            self.activityLocationResolve.hidden = YES;
            
            Register2ViewController * viewController = self.app.register2ViewController;
             
             ((Register2ViewController *) viewController).labelNeighbourhood.text = neighbourhood;

            [self.app changeRootViewController:viewController];
            
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];

   
    UIDatePicker *myPicker = [UIDatePicker new];
    
    //CustomDatePickerView *myPicker = [CustomDatePickerView new];
    
    //UIView *baseView = [[[UIView alloc] init] autorelease];
    
    //CustomDatePickerView *myPicker = [[CustomDatePickerView alloc] init];
    
    myPicker.datePickerMode = UIDatePickerModeDate;
    
    // sets the input method for the date field as a keyboard
    self.textFieldBirthdate.inputView = myPicker;

    [myPicker addTarget:self action:@selector(updateTextField:)
         forControlEvents:UIControlEventValueChanged];
    //[self.textFieldBirthdate becomeFirstResponder];
/*
    self.textFieldUsername.text = @"tlaskows";
    self.textFieldPassword.text = @"secret";
    self.textFieldEmail.text = @"tlaskows@gmail.com";
    
    self.selectorGender.selectedSegmentIndex = 0;
*/
    //[self.view addSubview: myPicker];
    // Do any additional setup after loading the view.
}

-(void)updateTextField:(UIDatePicker *)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"dd - MM - YYYY"];
    self.textFieldBirthdate.text = [dateFormatter stringFromDate:sender.date];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
