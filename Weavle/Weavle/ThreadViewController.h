//
//  ThreadViewController.h
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-21.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThreadViewController : UIViewController <UITableViewDataSource, UIAlertViewDelegate>

@property (strong, nonatomic) NSMutableArray* threadArray;

@end
