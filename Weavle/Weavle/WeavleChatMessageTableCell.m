//
//  WeavleChatMessageTableCell.m
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-17.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import "WeavleChatMessageTableCell.h"
#import "WeavleChatLabel.h"
#import "WeavleChatLabel2.h"

@implementation WeavleChatMessageTableCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)resizeHeight
{
    if ([self.messageReceivedLabel.text isEqualToString:@""] == YES)
    {
        [self.messageSentLabel resizeHeight];
    }
    else
    {
        [self.messageReceivedLabel resizeHeight];
    }
    //self.frame = CGRectMake(0, 0, 320, self.messageSentLabel.textHeight);
}

- (int)getCellHeight
{
    if ([self.messageReceivedLabel.text isEqualToString:@""] == YES)
    {
        NSLog(@"chat label height is: %d", self.messageSentLabel.textHeight);
        return self.messageSentLabel.textHeight;
    }
    else
    {
        NSLog(@"chat label height is: %d", self.messageReceivedLabel.textHeight);
        return self.messageReceivedLabel.textHeight;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
