//
//  ChatViewController.h
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-17.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SendMessageTextField.h"

@interface ChatViewController : UIViewController <UITableViewDataSource, UITextFieldDelegate>

@property (strong, nonatomic) NSMutableArray* messageArray;
@property (strong, nonatomic) NSString *myUserId;
@property (strong, nonatomic) SendMessageTextField *activeTextField;
@property (strong, nonatomic) IBOutlet SendMessageTextField *textFieldMessage;
@property (strong, nonatomic) IBOutlet UIButton *buttonSend;
@property (assign, nonatomic) int cellHeight;
@property (assign, nonatomic) int threadId;

- (IBAction)sendMessage:(id)sender;

@end
