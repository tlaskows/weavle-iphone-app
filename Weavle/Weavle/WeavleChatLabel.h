//
//  WeavleChatLabel.h
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-20.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeavleChatLabel : UILabel

- (void)resizeHeight;
@property (assign, nonatomic) int textHeight;

@end
