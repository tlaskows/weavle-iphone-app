//
//  HomepageViewController.m
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-14.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import "HomepageViewController.h"
#import "WeavleLocationManager.h"
#import "AppDelegate.h"

@interface HomepageViewController ()

@property (strong, nonatomic) WeavleLocationManager *weavleLocationManager;

@property (weak, nonatomic) IBOutlet UIButton *buttonHood;
@property (weak, nonatomic) IBOutlet UIButton *buttonMail;
@property (weak, nonatomic) IBOutlet UIButton *buttonFriends;
@property (weak, nonatomic) IBOutlet UIButton *buttonSettings;
@property (weak, nonatomic) IBOutlet UIButton *buttonExclamation;
@property (strong, nonatomic) AppDelegate *app;

@end

@implementation HomepageViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        // Custom initialization
        
        if (!self.app)
        { self.app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        }
    }
    
    return self;
}

- (IBAction)buttonThreadClicked:(id)sender
{
    UIViewController *viewController = self.app.threadViewController;
    
    [self.app changeRootViewController:viewController];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background blank.png"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.labelNeighbourhood.text = @"Please wait...";
    
    self.weavleLocationManager = [[WeavleLocationManager alloc] init];
    
    [self.weavleLocationManager startToGetCurrentLocation:nil];
    
    NSLog(@"isDoneResolving: %d", [self.weavleLocationManager isDoneResolving]);
    
    NSTimer *locationResolveTimer = [NSTimer scheduledTimerWithTimeInterval: 0.2
                                                                     target: self
                                                                   selector:@selector(onLocationResolveCheck:)
                                                                   userInfo: nil repeats:YES];
    
    self.buttonHood.transform = CGAffineTransformMakeScale(0.1,0.1);
    self.buttonHood.alpha = 0.0f;
    
    [UIView beginAnimations:@"button1" context:nil];
    [UIView setAnimationDuration:1];
    self.buttonHood.transform = CGAffineTransformMakeScale(1,1);
    self.buttonHood.alpha = 1.0f;
    [UIView commitAnimations];
    
    self.buttonMail.transform = CGAffineTransformMakeScale(0.1,0.1);
    self.buttonMail.alpha = 0.0f;
    
    [UIView beginAnimations:@"button2" context:nil];
    [UIView setAnimationDuration:1];
    self.buttonMail.transform = CGAffineTransformMakeScale(1,1);
    self.buttonMail.alpha = 1.0f;
    [UIView commitAnimations];

    self.buttonFriends.transform = CGAffineTransformMakeScale(0.1,0.1);
    self.buttonFriends.alpha = 0.0f;
    
    [UIView beginAnimations:@"button3" context:nil];
    [UIView setAnimationDuration:1];
    self.buttonFriends.transform = CGAffineTransformMakeScale(1,1);
    self.buttonFriends.alpha = 1.0f;
    [UIView commitAnimations];

    self.buttonSettings.transform = CGAffineTransformMakeScale(0.1,0.1);
    self.buttonSettings.alpha = 0.0f;
    
    [UIView beginAnimations:@"button4" context:nil];
    [UIView setAnimationDuration:1];
    self.buttonSettings.transform = CGAffineTransformMakeScale(1,1);
    self.buttonSettings.alpha = 1.0f;
    [UIView commitAnimations];

    self.buttonExclamation.transform = CGAffineTransformMakeScale(0.1,0.1);
    self.buttonExclamation.alpha = 0.0f;
    
    [UIView beginAnimations:@"button5" context:nil];
    [UIView setAnimationDuration:1];
    self.buttonExclamation.transform = CGAffineTransformMakeScale(1,1);
    self.buttonExclamation.alpha = 1.0f;
    [UIView commitAnimations];

}

-(void)onLocationResolveCheck:(NSTimer *)timer
{
    //  check to see if we are done waiting for the location
    if ([self.weavleLocationManager isDoneResolving] == YES)
    {
        NSLog(@"DONE RESOLVING LOCATION!!!");
        
        NSLog(@"Longitute is: %@", [self.weavleLocationManager getLongitude]);
        self.longitude = [self.weavleLocationManager getLongitude];
        
        NSLog(@"Latitude is: %@", [self.weavleLocationManager getLatitude]);
        self.latitude = [self.weavleLocationManager getLatitude];
        
        NSLog(@"City is: %@", [self.weavleLocationManager getCity]);
        self.city = [self.weavleLocationManager getCity];
        
        NSLog(@"Province/State is: %@", [self.weavleLocationManager getProvinceOrState]);
        self.province = [self.weavleLocationManager getProvinceOrState];
        
        NSLog(@"Country is: %@", [self.weavleLocationManager getCountry]);
        self.country = [self.weavleLocationManager getCountry];
        
        [timer invalidate];
        
        NSString *neighbourhood = [NSString stringWithFormat:@"%@\n%@\n%@", self.city, self.province, self.country];
        
        self.labelNeighbourhood.text = neighbourhood;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
