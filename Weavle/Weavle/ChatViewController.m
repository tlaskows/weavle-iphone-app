//
//  ChatViewController.m
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-17.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import "ChatViewController.h"
#import "WeavleChatMessageTableCell.h"
#import "AppDelegate.h"

@interface ChatViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UITextField *messageEditField;

@property (strong, nonatomic) IBOutlet UITableView *historicalMessagesTableView;
@property (strong, nonatomic) AppDelegate *app;
@property (readwrite, assign) int serverResponseTimeoutCount;
@property (strong, nonatomic) NSTimer *serverTimeoutTimer;
@end

@implementation ChatViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        // Custom initialization
        
        if (!self.app)
        {
            self.app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        }
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    //self.navigationItem.title = self.chatMateId;
    self.messageArray = [[NSMutableArray alloc] init];
    self.historicalMessagesTableView.rowHeight = UITableViewAutomaticDimension;
//    [self retrieveMessagesFromParseWithChatMateID:self.chatMateId];
    UITapGestureRecognizer *tapTableGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnTableView)];
    [self.historicalMessagesTableView addGestureRecognizer:tapTableGR];
    [self registerForKeyboardNotifications];
    
    //self.automaticallyAdjustsScrollViewInsets=NO;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.messageArray = [[NSMutableArray alloc] init];
    
    // TODO test code to see how threads are formatted... remove soon
    
    NSString * messagesString=[NSString stringWithFormat:@"http://www.thomaslaskowski.com:6969/weavle/v1/users/%d/message_threads/%d?pageNumber=0&pageSize=10", self.app.userId, self.threadId];
    
    [self.app.postHelper sendPostRequest: messagesString withJSONdata: nil withType:@"GET"];
    
    NSString * testString;
    
    testString = @"test";
    
    testString = @"test2";
    
    self.serverResponseTimeoutCount = 0;
    
    self.serverTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval: 2.0
                                                                   target: self
                                                                 selector:@selector(onServerResponseCheck:)
                                                                 userInfo: nil repeats:YES];
}

-(void)onServerResponseCheck:(NSTimer *)timer
{
    //  check to see if we are done waiting for the location
    if ([self.app.postHelper isDoneReceiving] == YES)
    {
        NSLog(@"GOT A RESPONSE FROM THE SERVER!!!");
        
        //[timer invalidate];
        self.messageArray = [[NSMutableArray alloc] init];
        
        // look at the response
        NSMutableData *serverResponseData = [self.app.postHelper getResponseData];
        
        NSError* error;
        NSDictionary* serverResponseJSON = [NSJSONSerialization JSONObjectWithData:serverResponseData
                                                                           options:kNilOptions
                                                                             error:&error];
        
        // TODO: if parsing the server reponse doesn't work, do something here...
        
        NSArray *messages = [serverResponseJSON objectForKey:@"messageList"];
        
        for (int i=[messages count]-1; i>=0; i--)
        {
            NSDictionary *message = messages[i];
            
            [self.messageArray addObject:message];
        }
        //self.activeTextField.canResign = NO;
        [self.historicalMessagesTableView reloadData];
        
        NSString * messagesString=[NSString stringWithFormat:@"http://www.thomaslaskowski.com:6969/weavle/v1/users/%d/message_threads/%d?pageNumber=0&pageSize=10", self.app.userId, self.threadId];
        
        [self.app.postHelper sendPostRequest: messagesString withJSONdata: nil withType:@"GET"];
        //self.activeTextField.canResign = YES;
    }
}

- (void)didTapOnTableView {
    [self.activeTextField resignFirstResponder];
}

// Setting up keyboard notifications.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}



// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
//    UIEdgeInsets contentInsets = UIEdgeInsetsMake(kbSize.height, 0.0, kbSize.height, 0.0);
//    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    
    
//    self.scrollView.contentInset = contentInsets;
//    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    //self.scrollView.frame.size.height-=kbSize.height;
    
    self.scrollView.frame = CGRectMake(0,65,self.scrollView.frame.size.width,503-kbSize.height);

    self.buttonSend.frame = CGRectMake(self.buttonSend.frame.origin.x,466-kbSize.height,self.buttonSend.frame.size.width,self.buttonSend.frame.size.height);
    
    self.textFieldMessage.frame = CGRectMake(self.textFieldMessage.frame.origin.x,466-kbSize.height,self.textFieldMessage.frame.size.width,self.textFieldMessage.frame.size.height);
    
    [self scrollTableToBottom];
//    self.scrollView.contentInset = contentInsets;
//    self.scrollView.scrollIndicatorInsets = contentInsets;
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
//    CGRect aRect = self.scrollView.frame;
//    aRect.size.height -= kbSize.height;
/*    if (!CGRectContainsPoint(aRect, self.activeTextField.frame.origin) ) {
        [self.scrollView scrollRectToVisible:self.activeTextField.frame animated:NO];
    }*/
    
   // [self.scrollView setAutoresizesSubviews:YES];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
/*    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;*/
    
    self.scrollView.frame = CGRectMake(0,65,self.scrollView.frame.size.width,503);
    
    self.buttonSend.frame = CGRectMake(self.buttonSend.frame.origin.x,466,self.buttonSend.frame.size.width,self.buttonSend.frame.size.height);
    
    self.textFieldMessage.frame = CGRectMake(self.textFieldMessage.frame.origin.x,466,self.textFieldMessage.frame.size.width,self.textFieldMessage.frame.size.height);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextFieldDelegate methods

- (void)textFieldDidBeginEditing:(SendMessageTextField *)textField
{
    self.activeTextField = textField;
}

- (void)textFieldDidEndEditing:(SendMessageTextField *)textField
{
    self.activeTextField = nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.messageArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     WeavleChatMessageTableCell *messageCell = [tableView dequeueReusableCellWithIdentifier:@"MessageListPrototypeCell" forIndexPath:indexPath];
    [self configureCell:messageCell forIndexPath:indexPath];
    
    return messageCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.cellHeight+20;
}

#pragma mark Method to configure the appearance of a message list prototype cell

- (void)configureCell:(WeavleChatMessageTableCell *)messageCell forIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableDictionary *chatMessage = self.messageArray[indexPath.row];
    
  /*  if ([[chatMessage senderId] isEqualToString:self.myUserId]) {
        // If the message was sent by myself
        messageCell.chatMateMessageLabel.text = @"";
        messageCell.myMessageLabel.text = chatMessage.text;
    } else {
        // If the message was sent by the chat mate
        messageCell.myMessageLabel.text = @"";
        messageCell.chatMateMessageLabel.text = chatMessage.text;
    }*/
    
    if ([[chatMessage objectForKey:@"sentByUserId"] integerValue] == self.app.userId)
    {
        messageCell.messageSentLabel.text = [chatMessage objectForKey:@"body"];
        [messageCell.messageSentLabel resizeHeight];
        self.cellHeight=[messageCell getCellHeight];
        //self.textFieldMessage.text = @"";
        messageCell.messageReceivedLabel.hidden = YES;
        messageCell.messageSentLabel.hidden = NO;
    }
    else
    {
        messageCell.messageReceivedLabel.text = [chatMessage objectForKey:@"body"];
        [messageCell.messageReceivedLabel resizeHeight];
        self.cellHeight=[messageCell getCellHeight];
        //self.textFieldMessage.text = @"";
        messageCell.messageSentLabel.hidden = YES;
        messageCell.messageReceivedLabel.hidden = NO;
    }
}

-(void)sendMessage:(id)sender
{
/*    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate sendTextMessage:self.messageEditField.text toRecipient:self.chatMateId];*/
    //[self.messageArray addObject:self.textFieldMessage.text];
    //[self.historicalMessagesTableView reloadData];
    //[self scrollTableToBottom];
    //http://www.thomaslaskowski.com:6969/weavle/v1/users/{userId}/message_threads/{threadId}/send_message
    [self.serverTimeoutTimer invalidate];
    
    NSMutableDictionary *message = [[NSMutableDictionary alloc]init];
    
    [message setObject:self.textFieldMessage.text forKey:@"body"];
    [message setObject:@"0" forKey:@"sentByUserId"];
    [message setObject:@"user" forKey:@"sentByUsername"];
    [message setObject:@"Test user" forKey:@"sentByFullName"];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:message options:NSJSONWritingPrettyPrinted error:&error];
    
    if (!jsonData)
    {
        NSLog(@"Error creating JSON data %@", error);
    }
    else
    {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSLog(@"JSON data is: %@ ", jsonString);
    }
    
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    }

    NSString * messagesString=[NSString stringWithFormat:@"http://www.thomaslaskowski.com:6969/weavle/v1/users/%d/message_threads/%d/send_message", self.app.userId, self.threadId];
    
    [self.app.postHelper sendPostRequest: messagesString withJSONdata: jsonData withType:@"POST"];
    
    self.serverResponseTimeoutCount = 0;
    
    self.serverTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval: 0.2
                                                                   target: self
                                                                 selector:@selector(onServerResponseCheck2:)
                                                                 userInfo: nil repeats:YES];
}

-(void)onServerResponseCheck2:(NSTimer *)timer
{
    //  check to see if we are done waiting for the location
    if ([self.app.postHelper isDoneReceiving] == YES)
    {
        NSLog(@"GOT A RESPONSE FROM THE SERVER!!!");
        
        [timer invalidate];
        
        // look at the response
        NSMutableData *serverResponseData = [self.app.postHelper getResponseData];
        
        NSError* error;
        NSDictionary* serverResponseJSON = [NSJSONSerialization JSONObjectWithData:serverResponseData
                                                                           options:kNilOptions
                                                                             error:&error];
        
        NSLog(@"thread response create is: %@: \n", serverResponseJSON);
        
        // TODO: if parsing the server reponse doesn't work, do something here...
        if ([[serverResponseJSON objectForKey:@"id"] integerValue] > 0)
        {
            NSLog(@"Message sent successfully!");
            
            NSString * messagesString=[NSString stringWithFormat:@"http://www.thomaslaskowski.com:6969/weavle/v1/users/%d/message_threads/%d?pageNumber=0&pageSize=10", self.app.userId, self.threadId];
            
            [self.app.postHelper sendPostRequest: messagesString withJSONdata: nil withType:@"GET"];
            
            NSString * testString;
            
            testString = @"test";
            
            testString = @"test2";
            
            self.serverResponseTimeoutCount = 0;
            
            self.serverTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval: 2.0
                                                                       target: self
                                                                     selector:@selector(onServerResponseCheck:)
                                                                     userInfo: nil repeats:YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Message could not be sent" delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
            return;
        }
    }
    else
    {
        self.serverResponseTimeoutCount++;
    }
    
    // wait up to 2 seconds for the server to return response data
    if (self.serverResponseTimeoutCount >= 10)
    {
        NSLog(@"The server timed out...");
        
        [timer invalidate];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"The Weavle server has timed out."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
}

- (void)scrollTableToBottom {
    int rowNumber = [self.historicalMessagesTableView numberOfRowsInSection:0];
    if (rowNumber > 0) [self.historicalMessagesTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:rowNumber-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
