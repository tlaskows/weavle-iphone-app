//
//  ViewController.m
//  Weavle
//
//  Created by Thomas Laskowski on 2015-08-27.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"

@interface LoginViewController ()

@property (strong, nonatomic) IBOutlet UITextField *textFieldUsername;
@property (strong, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (readwrite, assign) int serverResponseTimeoutCount;
@property (strong, nonatomic) AppDelegate *app;

@end

@implementation LoginViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        // Custom initialization
        
        if (!self.app)
        {
            self.app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        }
    }
    
    return self;
}

- (IBAction)buttonRegisterClicked:(id)sender
{
    UIViewController *viewController = self.app.registerViewController;
    
    [self.app changeRootViewController:viewController];
}


// to dismiss the keyboard when clicked on the screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonLoginClicked:(id)sender
{
    // check for blank fields
    if ([self.textFieldUsername.text  isEqual: @""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"The username field cannot be blank"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    else if ([self.textFieldPassword.text isEqual: @""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"The password field cannot be blank"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    NSString * loginString=[NSString stringWithFormat:@"http://www.thomaslaskowski.com:6969/weavle/v1/login?username=%@&password=%@", self.textFieldUsername.text, self.textFieldPassword.text];
    
    [self.app.postHelper sendPostRequest: loginString withJSONdata: nil withType:@"POST"];

    self.serverResponseTimeoutCount = 0;
    
    NSTimer *serverTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval: 0.2
                                                                   target: self
                                                                 selector:@selector(onServerResponseCheck:)
                                                                 userInfo: nil repeats:YES];
}

-(void)onServerResponseCheck:(NSTimer *)timer
{
    //  check to see if we are done waiting for the location
    if ([self.app.postHelper isDoneReceiving] == YES)
    {
        NSLog(@"GOT A RESPONSE FROM THE SERVER!!!");
        
        [timer invalidate];
        
        // look at the response
        NSMutableData *serverResponseData = [self.app.postHelper getResponseData];
        
        NSError* error;
        NSDictionary* serverResponseJSON = [NSJSONSerialization JSONObjectWithData:serverResponseData
                                                                           options:kNilOptions
                                                                             error:&error];
        
        // TODO: if parsing the server reponse doesn't work, do something here...
        
        NSLog(@"success is: %@", [serverResponseJSON objectForKey:@"isSuccessful"]);
        
        if ([[NSString stringWithFormat:@"%@", [serverResponseJSON objectForKey:@"isSuccessful"]] isEqualToString:@"0"] == YES)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:(NSString *)[serverResponseJSON objectForKey:@"message"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            return;
        }
        else
        {
            UIViewController *viewController = self.app.homepageViewController;
            
            self.app.userId = (int)[[serverResponseJSON objectForKey:@"userId"] intValue];
            
            self.app.neighbourhoodId = (int)[[serverResponseJSON objectForKey:@"neighbourhoodId"] intValue];
            
            
            [self.app changeRootViewController:viewController];
            
            return;
        }
    }
    else
    {
        self.serverResponseTimeoutCount++;
    }
    
    // wait up to 2 seconds for the server to return response data
    if (self.serverResponseTimeoutCount >= 10)
    {
        NSLog(@"The server timed out...");
        
        [timer invalidate];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"The Weavle server has timed out."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
}

@end
