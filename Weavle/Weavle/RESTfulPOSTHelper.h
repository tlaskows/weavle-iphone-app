//
//  RESTfulPOSTHelper.h
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-07.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RESTfulPOSTHelper : NSObject <NSURLConnectionDataDelegate>

- (BOOL) isDoneReceiving;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *receivedData;

- (void) sendPostRequest: (NSString *) url withJSONdata:(NSData *)jsonData withType: (NSString *)requestType;
- (NSMutableData *) getResponseData;

// - (void) drawRoundedRect:(NSRect)aRect inView:(NSView *)aView withColor:(NSColor *)color fill:(BOOL)fill

@end
