//
//  FriendsViewController.h
//  Weavle
//
//  Created by Thomas Laskowski on 2015-10-22.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@end
