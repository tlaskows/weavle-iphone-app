//
//  ViewControllerRegisterViewController.h
//  Weavle
//
//  Created by Thomas Laskowski on 2015-08-27.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *textFieldBirthdate;

@property (weak, nonatomic) IBOutlet UITextField *textFieldUsername;

@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;

@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;

@property (weak, nonatomic) IBOutlet UISegmentedControl *selectorGender;

@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *province;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLocationResolve;
- (NSString *) getBirthdate;
- (NSString *) getGender;

@end


