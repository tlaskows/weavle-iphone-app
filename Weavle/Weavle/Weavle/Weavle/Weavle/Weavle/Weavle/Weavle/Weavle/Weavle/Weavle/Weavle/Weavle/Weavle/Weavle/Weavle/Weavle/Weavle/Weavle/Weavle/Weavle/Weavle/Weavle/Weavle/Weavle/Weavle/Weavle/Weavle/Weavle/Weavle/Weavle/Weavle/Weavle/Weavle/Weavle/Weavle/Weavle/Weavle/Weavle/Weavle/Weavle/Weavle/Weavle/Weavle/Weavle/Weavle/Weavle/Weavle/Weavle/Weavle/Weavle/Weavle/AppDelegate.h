//
//  AppDelegate.h
//  Weavle
//
//  Created by Thomas Laskowski on 2015-08-27.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "RegisterViewController.h"
#import "Register2ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

- (void)changeRootViewController:(UIViewController*)viewController;

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;
@property (strong, nonatomic) RegisterViewController *registerViewController;
@property (strong, nonatomic) Register2ViewController *register2ViewController;

@end

