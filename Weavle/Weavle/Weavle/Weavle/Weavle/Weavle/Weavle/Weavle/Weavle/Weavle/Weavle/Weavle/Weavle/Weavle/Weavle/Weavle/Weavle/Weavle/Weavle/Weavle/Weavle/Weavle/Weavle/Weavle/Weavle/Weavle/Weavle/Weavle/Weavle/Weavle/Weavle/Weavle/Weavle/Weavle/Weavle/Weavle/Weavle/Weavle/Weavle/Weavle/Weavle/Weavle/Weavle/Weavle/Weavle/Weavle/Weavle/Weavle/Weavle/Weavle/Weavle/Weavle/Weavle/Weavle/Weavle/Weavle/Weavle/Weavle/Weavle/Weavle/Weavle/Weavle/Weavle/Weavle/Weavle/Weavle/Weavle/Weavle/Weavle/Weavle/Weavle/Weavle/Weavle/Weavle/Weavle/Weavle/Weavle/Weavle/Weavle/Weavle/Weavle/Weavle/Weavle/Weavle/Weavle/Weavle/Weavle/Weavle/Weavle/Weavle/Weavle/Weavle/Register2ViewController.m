//
//  Register2ViewController.m
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-02.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import "Register2ViewController.h"
#import "RegisterViewController.h"

#import "AppDelegate.h"

@interface Register2ViewController ()

@end

@implementation Register2ViewController

- (IBAction)buttonBackClicked:(id)sender
{
    UIViewController *viewController = ((AppDelegate *)[UIApplication sharedApplication].delegate).registerViewController;
    
    //[self presentViewController:viewController animated:YES completion:nil];
    
//    ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController = viewController;
    
    AppDelegate *app;
    
    if (!app) { app = (AppDelegate *)[[UIApplication sharedApplication] delegate]; }
    [app changeRootViewController:viewController];
}

- (IBAction)buttonRegisterClicked:(id)sender
{
/*    NSLog(@"Register button clicked!!!");
    
    NSLog(@"The country is: %@", self.registerViewController.country);
    
    NSLog(@"The country is: %@", [self.registerViewController getCountry]);
    
    NSLog(@"The username is: %@", [self.registerViewController getUsername]);*/
    
    // TODO: see if creating a helper function will be more practical
    NSDictionary *locationData =
    @{@"nickname" : @"The hood",
      @"imageURL" : @"www.thomaslaskowski.com",
      @"city" : self.registerViewController.city,
      @"province" : self.registerViewController.province,
      @"country" : self.registerViewController.country,
      };
    
    NSDictionary *registrationData =
    @{@"username": self.registerViewController.textFieldUsername.text,
      @"password": self.registerViewController.textFieldPassword.text,
      @"firstName": @"anonymous",
      @"lastName" : @"anonymous",
      @"gender" : [self.registerViewController getGender],
      @"email" : self.registerViewController.textFieldEmail.text,
      @"unixDateOfBirth" : @"100",
      @"neighbourhood" : locationData,
      };
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:registrationData options:NSJSONWritingPrettyPrinted error:&error];
    
    if (!jsonData)
    {
        NSLog(@"Error creating JSON data %@", error);
    }
    else
    {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSLog(@"JSON data is: %@ ", jsonString);
    }
    
    // TODO: create a helper class to handle POST connections
    //if there is a connection going on just cancel it.
    // [self.connection cancel];
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
//    [data release];
    
    //initialize url that is going to be fetched.
    NSURL *url = [NSURL URLWithString:@"http://localhost:8080/weavle/v1/users/register"];
    
    //http://localhost:8080/weavle/v1/login
    
    //NSURL *url = [NSURL URLWithString:@"http://localhost:8080/weavle/v1/login"];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    
    // init the json string
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    //initialize a post data
    NSString *postData = [[NSString alloc] initWithString:jsonString];
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    //set post data of request
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    //initialize a connection from request
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    
    [connection start];
}

//- (void) setRegisterViewController:(RegisterViewController *)registerViewController
//{
//    self.registerViewController = registerViewController;
//}

/*
 this method might be calling more than one times according to incoming data size
 */
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
}
/*
 if there is an error occured, this method will be called by connection
 */
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    NSLog(@"%@" , error);
}

/*
 if data is successfully received, this method will be called by connection
 */
-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    //initialize convert the received data to string with UTF8 encoding
    NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                                              encoding:NSUTF8StringEncoding];
    NSLog(@"%@" , htmlSTR);
    
    NSError *e = nil;
    
    NSDictionary *myJSONSerializer = [NSJSONSerialization JSONObjectWithData:self.receivedData options:NSJSONWritingPrettyPrinted error:&e];
    
    NSLog(@"jsonDictionary - %@", myJSONSerializer);
}

- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.registerViewController = ((AppDelegate *)[UIApplication sharedApplication].delegate).registerViewController;

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    
    NSString *neighbourhood = [NSString stringWithFormat:@"%@\n%@\n%@", self.registerViewController.city, self.registerViewController.province, self.registerViewController.country];
    
    self.labelNeighbourhood.text = neighbourhood;
    
//    NSLog(@"The country is: %@", [self.registerViewController getCountry]);
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
