//
//  ViewController.m
//  Weavle
//
//  Created by Thomas Laskowski on 2015-08-27.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickmePressed:(id)sender
{
/*    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController *viewController = (ViewController *)[storyboard instantiateViewControllerWithIdentifier:@"RegisterScreen"];
    [self presentViewController:viewController animated:YES completion:nil];*/
    
    UIViewController *viewController = ((AppDelegate *)[UIApplication sharedApplication].delegate).registerViewController;
    
    //[self presentViewController:viewController animated:YES completion:nil];
    
    /*((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController = viewController;*/
    
    //self.view.window.rootViewController = viewController;
    
    AppDelegate *app;
    
    if (!app) { app = (AppDelegate *)[[UIApplication sharedApplication] delegate]; }
    [app changeRootViewController:viewController];
}


@end
