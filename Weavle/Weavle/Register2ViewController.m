//
//  Register2ViewController.m
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-02.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import "Register2ViewController.h"
#import "RegisterViewController.h"
#import "RESTfulPOSTHelper.h"

#import "AppDelegate.h"

@interface Register2ViewController ()

@property (strong, nonatomic) RESTfulPOSTHelper *postHelper;
@property (assign, nonatomic) int serverResponseTimeoutCount;
@property (strong, nonatomic) AppDelegate *app;

@end

@implementation Register2ViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        // Custom initialization
        //self.postHelper = [[RESTfulPOSTHelper alloc] init];
        if (!self.app)
        {
            self.app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        }
    }
    
    return self;
}

- (IBAction)buttonBackClicked:(id)sender
{
    UIViewController *viewController = self.app.registerViewController;
    
    [self.app changeRootViewController:viewController];
}

- (IBAction)buttonRegisterClicked:(id)sender
{
    // TODO: see if creating a helper function will be more practical
    NSDictionary *locationData =
    @{@"nickname" : @"The hood",
      @"imageURL" : @"www.thomaslaskowski.com",
      @"city" : self.registerViewController.city,
      @"province" : self.registerViewController.province,
      @"country" : self.registerViewController.country,
      };
    
    NSDictionary *registrationData =
    @{@"username": self.registerViewController.textFieldUsername.text,
      @"password": self.registerViewController.textFieldPassword.text,
      @"firstName": @"anonymous",
      @"lastName" : @"anonymous",
      @"gender" : [self.registerViewController getGender],
      @"email" : self.registerViewController.textFieldEmail.text,
      @"unixDateOfBirth" : @"100",
      @"neighbourhood" : locationData,
      };
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:registrationData options:NSJSONWritingPrettyPrinted error:&error];
    
    if (!jsonData)
    {
        NSLog(@"Error creating JSON data %@", error);
    }
    else
    {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSLog(@"JSON data is: %@ ", jsonString);
    }
    
    // TODO: put the init to the POST helper in this classes's init
    
    [self.app.postHelper sendPostRequest: @"http://www.thomaslaskowski.com:6969/weavle/v1/auth/register" withJSONdata: jsonData withType:@"POST"];
    
    self.serverResponseTimeoutCount = 0;
    
    NSTimer *serverTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval: 0.2
                                                                     target: self
                                                                   selector:@selector(onServerResponseCheck:)
                                                                   userInfo: nil repeats:YES];
    
    // TODO: create a helper class to handle POST connections
    //if there is a connection going on just cancel it.
    // [self.connection cancel];
    
    //initialize new mutable data
/*    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    //initialize url that is going to be fetched.
    NSURL *url = [NSURL URLWithString:@"http://localhost:8080/weavle/v1/users/register"];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[url standardizedURL]];
    
    //set http method
    [request setHTTPMethod:@"POST"];
    
    // init the json string
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    //initialize a post data
    NSString *postData = [[NSString alloc] initWithString:jsonString];
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    //set post data of request
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    //initialize a connection from request
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    
    //start the connection
    
    [connection start];*/
}

-(void)onServerResponseCheck:(NSTimer *)timer
{
    //  check to see if we are done waiting for the location
    if ([self.app.postHelper isDoneReceiving] == YES)
    {
        NSLog(@"GOT A RESPONSE FROM THE SERVER!!!");
        
        [timer invalidate];
        
        // look at the response
        NSMutableData *serverResponseData = [self.app.postHelper getResponseData];
        
        NSError* error;
        NSDictionary* serverResponseJSON = [NSJSONSerialization JSONObjectWithData:serverResponseData
                                                             options:kNilOptions
                                                               error:&error];
        
        // TODO: if parsing the server reponse doesn't work, do something here...
        
        NSLog(@"success is: %@", [serverResponseJSON objectForKey:@"isSuccessful"]);
        
        if ([[NSString stringWithFormat:@"%@", [serverResponseJSON objectForKey:@"isSuccessful"]] isEqualToString:@"0"] == YES)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:(NSString *)[serverResponseJSON objectForKey:@"message"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            return;
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"Registration was successful."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
            UIViewController *viewController = self.app.homepageViewController;
            
            [self.app changeRootViewController:viewController];
            
            return;
        }
    }
    else
    {
        self.serverResponseTimeoutCount++;
    }
    
    // wait up to 2 seconds for the server to return response data
    if (self.serverResponseTimeoutCount >= 10)
    {
        NSLog(@"The server timed out...");
        
        [timer invalidate];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"The Weavle server has timed out."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.registerViewController = self.app.registerViewController;

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    
    NSString *neighbourhood = [NSString stringWithFormat:@"%@\n%@\n%@", self.registerViewController.city, self.registerViewController.province, self.registerViewController.country];
    
    self.labelNeighbourhood.text = neighbourhood;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
