//
//  SelectParticipantsViewController.m
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-19.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import "SelectParticipantsViewController.h"
#import "AppDelegate.h"

@interface SelectParticipantsViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UITableView *participantsTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBarParticipants;
@property (strong, nonatomic) IBOutlet UIButton *buttonCreateThread;
@property (readwrite, assign) int serverResponseTimeoutCount;
@property (strong, nonatomic) AppDelegate *app;
@end

@implementation SelectParticipantsViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        // Custom initialization
        
        if (!self.app)
        {
            self.app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        }
    }
    
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.participantArray = [[NSMutableArray alloc] init];
    
    // TODO test code to see how threads are formatted... remove soon
    
    NSString * participantString=[NSString stringWithFormat:@"http://www.thomaslaskowski.com:6969/weavle/v1/admins/users/basic_info"];
    
    [self.app.postHelper sendPostRequest: participantString withJSONdata: nil withType:@"GET"];
    
    NSString * testString;
    
    testString = @"test";
    
    testString = @"test2";
    
    self.serverResponseTimeoutCount = 0;
    
    NSTimer *serverTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval: 0.2
                                                                   target: self
                                                                 selector:@selector(onServerResponseCheck:)
                                                                 userInfo: nil repeats:YES];
}

-(void)onServerResponseCheck:(NSTimer *)timer
{
    //  check to see if we are done waiting for the location
    if ([self.app.postHelper isDoneReceiving] == YES)
    {
        NSLog(@"GOT A RESPONSE FROM THE SERVER!!!");
        
        [timer invalidate];
        
        // look at the response
        NSMutableData *serverResponseData = [self.app.postHelper getResponseData];
        
        NSError* error;
        NSDictionary* serverResponseJSON = [NSJSONSerialization JSONObjectWithData:serverResponseData
                                                                           options:kNilOptions
                                                                             error:&error];
        
        NSLog(@"users are: %@: \n", serverResponseJSON);
        
        // TODO: if parsing the server reponse doesn't work, do something here...
        
        NSArray *participants = [serverResponseJSON objectForKey:@"users"];
        
        for (int i=0; i<[participants count]; i++)
        {
            NSMutableDictionary *participant = [(NSDictionary *)participants[i] mutableCopy];
            
            [participant setObject:@"false" forKey:@"isChecked"];
            
            [self.participantArray addObject:participant];
        }
        [self.participantsTableView reloadData];
    }
    else
    {
        self.serverResponseTimeoutCount++;
    }
    
    // wait up to 2 seconds for the server to return response data
    if (self.serverResponseTimeoutCount >= 10)
    {
        NSLog(@"The server timed out...");
        
        [timer invalidate];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"The Weavle server has timed out."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
}

-(void)onServerResponseCheck2:(NSTimer *)timer
{
    //  check to see if we are done waiting for the location
    if ([self.app.postHelper isDoneReceiving] == YES)
    {
        NSLog(@"GOT A RESPONSE FROM THE SERVER!!!");
        
        [timer invalidate];
        
        // look at the response
        NSMutableData *serverResponseData = [self.app.postHelper getResponseData];
        
        NSError* error;
        NSDictionary* serverResponseJSON = [NSJSONSerialization JSONObjectWithData:serverResponseData
                                                                           options:kNilOptions
                                                                             error:&error];
        
        NSLog(@"thread response create is: %@: \n", serverResponseJSON);
        
        // TODO: if parsing the server reponse doesn't work, do something here...
        if ([[NSString stringWithFormat:@"%@", [serverResponseJSON objectForKey:@"isSuccessful"]] isEqualToString:@"1"] == YES)
        {
            [self.app changeRootViewController:self.app.threadViewController];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Thread could not be created" delegate:nil
                                cancelButtonTitle:@"OK"
                                otherButtonTitles:nil];
            [alert show];
            
            [self.app changeRootViewController:self.app.threadViewController];
            
            return;

        }

    }
    else
    {
        self.serverResponseTimeoutCount++;
    }
    
    // wait up to 2 seconds for the server to return response data
    if (self.serverResponseTimeoutCount >= 10)
    {
        NSLog(@"The server timed out...");
        
        [timer invalidate];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"The Weavle server has timed out."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
}


- (IBAction)buttonDoneClicked:(id)sender
{
    NSLog(@"Button done clicked!\n");
    
    NSMutableArray *participants = [[NSMutableArray alloc] init];
    
    for (int i=0; i<[self.participantArray count]; i++)
    {
        if ([[self.participantArray[i] objectForKey:@"isChecked"] isEqualToString:@"true"] == YES)
        {
            [participants addObject:[self.participantArray[i] objectForKey:@"id"]];
        }
    }
    
    NSMutableDictionary *thread = [[NSMutableDictionary alloc]init];
    
    [thread setObject:self.threadName forKey:@"title"];
    [thread setObject:@"false" forKey:@"isPrivateConversation"];
    [thread setObject:participants forKey:@"participantUserIds"];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:thread options:NSJSONWritingPrettyPrinted error:&error];
    
    if (!jsonData)
    {
        NSLog(@"Error creating JSON data %@", error);
    }
    else
    {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSLog(@"JSON data is: %@ ", jsonString);
    }
    
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    } else
    {
        NSString *request = [NSString stringWithFormat:@"http://www.thomaslaskowski.com:6969/weavle/v1/users/%d/conversations/create", self.app.userId];
        
        [self.app.postHelper sendPostRequest:request withJSONdata:jsonData withType:@"POST"];
        
        self.serverResponseTimeoutCount = 0;
        
        NSTimer *serverTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval: 0.2
                                                                       target: self
                                                                     selector:@selector(onServerResponseCheck2:)
                                                                     userInfo: nil repeats:YES];
    }
}

- (void) viewWillDisappear:(BOOL)animated {
    [self.participantsTableView deselectRowAtIndexPath:[self.participantsTableView indexPathForSelectedRow] animated:animated];
    [super viewWillDisappear:animated];
}

- (IBAction)buttonCancelClicked:(id)sender
{
    [self.app changeRootViewController:self.app.threadViewController];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
/*    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                               initWithTitle:@"Next" style:UIBarButtonItemStylePlain
                                               target:self action:@selector(nextControls)];
 */
    
    // Do any additional setup after loading the view.
    self.participantArray = [[NSMutableArray alloc] init];
    self.participantsTableView.rowHeight = UITableViewAutomaticDimension;
    
    [self.searchDisplayController.searchResultsTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"ParticipantListPrototypeCell"];
  //    [self retrieveMessagesFromParseWithChatMateID:self.chatMateId];
//    UITapGestureRecognizer *tapTableGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnTableView)];
//    [self.participantsTableView addGestureRecognizer:tapTableGR];
    [self registerForKeyboardNotifications];
    
/*    [self.participantArray addObject:@"Dannie"];
    [self.participantArray addObject:@"Dannie W"];
    [self.participantArray addObject:@"Dannie D"];
    [self.participantArray addObject:@"Tom"];
    [self.participantArray addObject:@"Thomas"];
    [self.participantArray addObject:@"Tomasz"];
    [self.participantArray addObject:@"Tomek"];
    [self.participantArray addObject:@"Lucifer1"];
    [self.participantArray addObject:@"Lucifer2"];
    [self.participantArray addObject:@"Lucifer3"];
    [self.participantArray addObject:@"God"];
    [self.participantArray addObject:@"Greg"];
    [self.participantArray addObject:@"Gregory"];
    [self.participantsTableView reloadData];
 */

}

- (void)contact
{
    NSLog(@"Next clicked!\n");
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)path
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:path];
    
/*    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        cell.accessoryType = UITableViewCellAccessoryNone;
    } else {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }*/
    
    NSMutableDictionary *cellItem;
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        
        cellItem = [self.filteredParticipantArray objectAtIndex:path.row];
    }
    else
    {
        cellItem = [self.participantArray objectAtIndex:path.row];
    }
    
    if ([[cellItem objectForKey:@"isChecked"] isEqualToString:@"false"])
    {
        [cellItem setObject:@"true" forKey:@"isChecked"];
    }
    else
    {
        [cellItem setObject:@"false" forKey:@"isChecked"];
    }
    
    [tableView reloadData];
    [self.participantsTableView reloadData];
}

- (void)didTapOnTableView {
    //[self.searchBarParticipants resignFirstResponder];
}

// Setting up keyboard notifications.
- (void)registerForKeyboardNotifications
{
/*    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];*/
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;

    self.scrollView.frame = CGRectMake(0,106,self.scrollView.frame.size.width,419-kbSize.height);
    
    self.buttonCreateThread.frame = CGRectMake(self.buttonCreateThread.frame.origin.x,533-kbSize.height,self.buttonCreateThread.frame.size.width,self.buttonCreateThread.frame.size.height);
    
 //   [self scrollTableToBottom];

}

- (void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView
{
    //tableView.frame = self.participantsTableView.frame;
    
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.scrollView.frame = CGRectMake(0,106,self.scrollView.frame.size.width,419);
    
    self.buttonCreateThread.frame = CGRectMake(self.buttonCreateThread.frame.origin.x,533,self.buttonCreateThread.frame.size.width,self.buttonCreateThread.frame.size.height);
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.participantsTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //return [self.participantArray count];
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
        return [self.filteredParticipantArray count];
    return [self.participantArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *messageCell = [tableView dequeueReusableCellWithIdentifier:@"ParticipantListPrototypeCell" forIndexPath:indexPath];
 //   [self configureCell:messageCell forIndexPath:indexPath];
    
    NSMutableDictionary *chatMessage;
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        /* searchResultsTableView code here */
        chatMessage = self.filteredParticipantArray[indexPath.row];
        
        messageCell.textLabel.text = [chatMessage objectForKey:@"username"];
    } else {
        /* Base tableView table code here */
        chatMessage = self.participantArray[indexPath.row];
        
        messageCell.textLabel.text = [chatMessage objectForKey:@"username"];
    }
    
    
    if ([[chatMessage objectForKey:@"isChecked"] isEqualToString:@"true"])
    {
        messageCell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        messageCell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return messageCell;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    //NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"SELF beginswith[c] %@", searchText];
    
//    NSPredicate *predicateString = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", keySelected, text];
    
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"%K beginswith[c] %@", @"username", searchText];
    
    self.filteredParticipantArray = [self.participantArray filteredArrayUsingPredicate:filterPredicate];
    NSLog(@"filteres participants: %@", self.filteredParticipantArray);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
