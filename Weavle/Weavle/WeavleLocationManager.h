//
//  WeavleLocationManager.h
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-01.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface WeavleLocationManager : NSObject

- (void)startToGetCurrentLocation:(id)sender;
- (BOOL)isDoneResolving;
- (NSString *) getLatitude;
- (NSString *) getLongitude;
- (NSString *) getCountry;
- (NSString *) getCity;
- (NSString *) getProvinceOrState;
@end
