//
//  FriendCollectionViewCell.h
//  Weavle
//
//  Created by Thomas Laskowski on 2015-10-22.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel * labelName;
@property (strong, nonatomic) IBOutlet UIImageView * image;
@end
