//
//  WeavleThreadTableCell.m
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-21.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import "WeavleThreadTableCell.h"

@implementation WeavleThreadTableCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
