//
//  WeavleChatMessageTableCell.h
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-17.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeavleChatLabel.h"
#import "WeavleChatLabel2.h"

@interface WeavleChatMessageTableCell : UITableViewCell

@property (strong, nonatomic) IBOutlet WeavleChatLabel *messageSentLabel;
@property (strong, nonatomic) IBOutlet WeavleChatLabel2 *messageReceivedLabel;

- (void)resizeHeight;
- (int)getCellHeight;
@end
