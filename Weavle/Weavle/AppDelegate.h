//
//  AppDelegate.h
//  Weavle
//
//  Created by Thomas Laskowski on 2015-08-27.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "Register2ViewController.h"
#import "HomepageViewController.h"
#import "ChatViewController.h"
#import "SelectParticipantsViewController.h"
#import "ThreadViewController.h"
#import "FriendsViewController.h"

#import "RESTFulPOSTHelper.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

- (void)changeRootViewController:(UIViewController*)viewController;

@property (strong, nonatomic) RESTfulPOSTHelper *postHelper;
@property (strong, nonatomic) NSURLConnection *connection;

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) LoginViewController *loginViewController;
@property (strong, nonatomic) RegisterViewController *registerViewController;
@property (strong, nonatomic) Register2ViewController *register2ViewController;
@property (strong, nonatomic) HomepageViewController *homepageViewController;

@property (strong, nonatomic) ChatViewController *chatViewController;

@property (strong, nonatomic) SelectParticipantsViewController *selectParticipantsViewController;

@property (strong, nonatomic) ThreadViewController *threadViewController;

@property (strong, nonatomic) FriendsViewController *friendsViewController;

@property (assign) int userId;
@property (assign) int neighbourhoodId;

@end

