//
//  RESTfulPOSTHelper.m
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-07.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import "RESTfulPOSTHelper.h"
#import "AppDelegate.h"

@interface RESTfulPOSTHelper ()

@property (nonatomic, assign) BOOL isDoneReceivingStatus;

@end

@implementation RESTfulPOSTHelper

// override init
// TODO: I think this isn't doing anything at this time...
- (id) init
{
    self = [super init];
    
    return self;
}

- (NSMutableData *) getResponseData
{
    return self.receivedData;
}

- (void) sendPostRequest: (NSString *) url withJSONdata:(NSData *)jsonData withType: (NSString *)requestType
{
    //[self.connection cancel];
    
    self.isDoneReceivingStatus = NO;
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
 
    //initialize url that is going to be fetched.
    NSURL *serverURL = [NSURL URLWithString:url];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[serverURL standardizedURL]];
    
    //set http method
    [request setHTTPMethod:requestType];
    
    // init the json string
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    //initialize a post data
    NSString *postData = [[NSString alloc] initWithString:jsonString];
    //set request content type we MUST set this value.
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"en" forHTTPHeaderField:@"languageCode"];
    
    //set post data of request
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    //initialize a connection from request
//    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    AppDelegate *app;
    
    if (!app) { app = (AppDelegate *)[[UIApplication sharedApplication] delegate]; }
    
  //  app.connection=connection;
    
    self.connection = app.connection;
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    //start the connection
    [self.connection start];
}

- (BOOL) isDoneReceiving
{
    return self.isDoneReceivingStatus;
}

/*
 this method might be calling more than one times according to incoming data size
 */
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
}
/*
 if there is an error occured, this method will be called by connection
 */
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    NSLog(@"%@" , error);
}

/*
 if data is successfully received, this method will be called by connection
 */
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    self.isDoneReceivingStatus = YES;
    
    //initialize convert the received data to string with UTF8 encoding
    NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                                              encoding:NSUTF8StringEncoding];
    NSLog(@"%@" , htmlSTR);
    
    NSError *e = nil;
    
    NSDictionary *myJSONSerializer = [NSJSONSerialization JSONObjectWithData:self.receivedData options:NSJSONWritingPrettyPrinted error:&e];
    
    NSLog(@"jsonDictionary - %@", myJSONSerializer);
}

@end
