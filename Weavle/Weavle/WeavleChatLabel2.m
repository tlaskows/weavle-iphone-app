//
//  WeavleChatLabel2.m
//  Weavle
//
//  Created by Thomas Laskowski on 2015-10-15.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import "WeavleChatLabel2.h"

@implementation WeavleChatLabel2

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)resizeHeight
{
    CGSize labelTextSize = [self.text sizeWithFont:self.font
                                 constrainedToSize:CGSizeMake(self.frame.size.width, MAXFLOAT)
                                     lineBreakMode:NSLineBreakByWordWrapping];
    
    self.frame = CGRectMake(5, 0, 320-85, labelTextSize.height+8);
    
    self.textHeight = labelTextSize.height;
}

- (void)drawTextInRect:(CGRect)rect
{
    UIEdgeInsets insets = {4, 8, 4, 8};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    //NSLog(@"init custom table cell");
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        // Initialization code here.
        //  self.layer.cornerRadius=4.0;
        //  self.layer.masksToBounds=YES;
        [self setNumberOfLines:0];
        //    [self sizeToFit];
        /*        [self setNumberOfLines:0];
         CGSize labelsize=[self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(250, 1000.0) lineBreakMode:UILineBreakModeWordWrap];
         int y=0;
         self.frame=CGRectMake(65, y, 245, labelsize.height);*/
        //self.adjustsFontSizeToFitWidth=YES;
        //self.layoutMargins=UIEdgeInsetsMake(0, 5.0, 0, 0);
    }
    return self;
}

@end
