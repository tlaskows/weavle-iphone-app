//
//  SelectParticipantsViewController.h
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-19.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectParticipantsViewController : UIViewController <UISearchBarDelegate, UISearchDisplayDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSMutableArray* participantArray;
@property (strong, nonatomic) NSArray* filteredParticipantArray;

@property (strong, nonatomic) NSString * threadName;
@end
