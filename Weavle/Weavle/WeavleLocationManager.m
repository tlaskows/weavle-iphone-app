//
//  WeavleLocationManager.m
//  Weavle
//
//  Created by Thomas Laskowski on 2015-09-01.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import "WeavleLocationManager.h"

@interface WeavleLocationManager () <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
/** The most recent current location, or nil if the current location is unknown, invalid, or stale. */
@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, strong) CLGeocoder *geocoder;
@property (nonatomic, retain) CLPlacemark *placemark;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, assign) BOOL isDoneResolvingStatus;
@end

@implementation WeavleLocationManager

- (BOOL) isDoneResolving
{
    return self.isDoneResolvingStatus;
}

- (NSString *) getLatitude
{
    return self.latitude;
}

- (NSString *) getLongitude
{
    return self.longitude;
}

- (NSString *) getCountry
{
    return self.placemark.country;
}

- (NSString *) getCity
{
    return self.placemark.locality;
}

- (NSString *) getProvinceOrState
{
    return self.placemark.administrativeArea;
}

- (void)startToGetCurrentLocation:(id)sender
{
    self.isDoneResolvingStatus = NO;
    
    self.locationManager.delegate = self;
    
    // TODO: need to decide on the accuracy we want as there is no GPS reception inside a building
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    // TODO: this needs to be requested when the app starts up
    [self.locationManager requestWhenInUseAuthorization];
    
    [self.locationManager startUpdatingLocation];
}

- (id)init
{
    self = [super init];
    
    self.locationManager = [[CLLocationManager alloc] init];
    
    self.geocoder = [[CLGeocoder alloc] init];
    
//    self.placemark = [[CLPlacemark alloc] init];
    
    return self;
}

// TODO:  I'm not sure this line is needed
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

// TODO:  what I really want to try is comparing the new location to the old one then stop the updates instead of waiting.  This may be faster.
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    //CLLocation *currentLocation = newLocation;
  
    if (oldLocation == nil) return;
    
    CLLocationDistance distanceThreshold = 2.0; // in meters
    
    if ([newLocation distanceFromLocation:oldLocation] < distanceThreshold)
    {
    
        self.currentLocation = newLocation;

        [self.locationManager stopUpdatingLocation];
        
        if (self.currentLocation != nil) {
            self.longitude = [NSString stringWithFormat:@"%.8f", self.currentLocation.coordinate.longitude];
            self.latitude = [NSString stringWithFormat:@"%.8f", self.currentLocation.coordinate.latitude];
        }
        
        
        // reverse geocoding
        NSLog(@"Resolving the Address");
        [self.geocoder reverseGeocodeLocation:self.currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
         {
             NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
             if (error == nil && [placemarks count] > 0)
             {
                 self.placemark = [placemarks lastObject];
             } else
             {
                 NSLog(@"%@", error.debugDescription);
             }
             
             self.isDoneResolvingStatus = YES;
             
         } ];
    }
    // TODO: We need to have the delay for location services user configurable
/*    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        // do something
        // Stop Location Manager
        [self.locationManager stopUpdatingLocation];
        
        if (self.currentLocation != nil) {
            self.longitude = [NSString stringWithFormat:@"%.8f", self.currentLocation.coordinate.longitude];
            self.latitude = [NSString stringWithFormat:@"%.8f", self.currentLocation.coordinate.latitude];
        }
*/
        // Reverse Geocoding
    
}

@end
