//
//  SendMessageTextField.m
//  Weavle
//
//  Created by Thomas Laskowski on 2015-10-15.
//  Copyright (c) 2015 Weavle. All rights reserved.
//

#import "SendMessageTextField.h"

@implementation SendMessageTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (BOOL)canResignFirstResponder
{
    return self.canResign;
}

@end
